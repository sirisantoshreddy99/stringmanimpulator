package sirisantosh;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * <h1>String Manipulator</h1> The StringManipulator program implements String
 * related logics where string is test if it is @see #isPalindrome, clean the
 * string by keeping only alpha characters using @see #cleanString method it
 * also read and write csv files
 * <p>
 *
 * @author Sri Santosh Reddy
 * @version 1.0
 * @since 03-05-2017
 */
public class StringManipulator {

	/*
	 * input file name with path where csv data is stored
	 */
	public static final String INPUT_CSV_FILE = "src/atdd/palindrome_tests.csv";

	/*
	 * output file name with path where processed data will be stored in comma
	 * separated csv file
	 */
	public static final String OUTPUT_CSV_FILE = "src/atdd/palindrome_sorted.csv";

	/**
	 * Cleans the string by keeping only alpha characters
	 * 
	 * @param str
	 *            This is the word needs to be cleaned.
	 */
	public String cleanString(String str) {
		if (str == null || str.length() == 0) {
			return "";
		}
		str = str.replaceAll("[^A-Za-z]", "");
		return str.toUpperCase();
	}

	/**
	 * Tests and find out if given word is Palindrome or not.
	 * 
	 * @param Word
	 *            to be tested for Palindrome.
	 * @return boolean flag, true if given word is Palindrome, false otherwise.
	 */
	public boolean isPalindrome(String word) {
		if (word == null || word.length() == 0) {
			return false;
		}
		return word.equals(new StringBuilder(word).reverse().toString());
	}

	/**
	 * Sort a large array of strings in alphabetical order.
	 * 
	 * @param Words
	 *            array to be sorted alphabetical order.
	 * @return String[] sorted words.
	 */
	public String[] sortStrings(String[] words) {
		if (words == null || words.length == 0) {
			return null;
		}
		Arrays.sort(words, String.CASE_INSENSITIVE_ORDER);
		return words;
	}

	/**
	 * read the CSV file, clean the words and changes into upper case. data is
	 * returned in String[] array
	 * 
	 * @return String[] words array from CSV file.
	 */
	public String[] readCSVFile() {

		BufferedReader br = null;
		String line = "";
		List<String> words = new ArrayList<String>();

		try {

			br = new BufferedReader(new FileReader(INPUT_CSV_FILE));
			boolean headerRow = true;
			while ((line = br.readLine()) != null) {
				if (headerRow == true) {
					// skip first row, 'Words' word will be allowed from second
					// row onwards
					headerRow = false;
					continue;
				}
				words.add(cleanString(line));
			}
			System.out.println("words= " + words);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}

		return words.toArray(new String[0]);
	}

	/**
	 * write the data in CSV file, first column is words and second column comma
	 * separated is boolean value based on Palindrome function.
	 * 
	 * @param Words
	 *            array to be stored in CSV file.
	 * 
	 */
	public void writeCSVFile(String[] words) {
		BufferedWriter wr = null;
		StringBuilder output = new StringBuilder();
		output.append("Words,Palindrome");
		output.append('\n');
		for (int i = 0; i < words.length; i++) {
			// will sort TRUE / FALSE in upper case, comma seperated will be in
			// different column
			output.append(words[i] + ", " + Boolean.toString(isPalindrome(words[i])).toUpperCase());
			output.append('\n');
		}

		try {

			wr = new BufferedWriter(new FileWriter(OUTPUT_CSV_FILE));
			wr.write(output.toString());
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (wr != null) {
				try {
					wr.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

	/**
	 * Main function which does all the job by calling different methods
	 * logically sequentially to read, sort and finally write to the output csv
	 * file.
	 */
	public void main() {
		String[] words = readCSVFile();
		String[] sortedWords = sortStrings(words);
		writeCSVFile(sortedWords);
	}

	public static void main(String[] args) {
		StringManipulator manipulator = new StringManipulator();
		/*
		 * System.out.println(manipulator.isPalindrome(manipulator.cleanString(
		 * "asdf#&ASDFf@#&dsaFDSA"))); String[] arr = {"ASDFASDFDSAFDSA",
		 * "ASDFASDFFDSAFDSA", "ASDFJDK", "COMPUTER", "NOON", "PICKLE",
		 * "RAINFOREST", "TAXI", "TRUST" };
		 * System.out.println(manipulator.sortStrings(arr));
		 */ manipulator.main();
	}
}
